﻿using System.Text;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using Parser.Models;
using Parser.Models.Requests;
using Parser.Models.Responses;

namespace Parser;

public class Handler
{
    public static void Main(string[] args)
    {
    }

    public async Task<Response> FunctionHandler(Request<object> request)
    {
        try
        {
            if (HttpMethod.Get.Method != request.httpMethod)
            {
                return new MethodNotAllowedResponse();
            }

            var userLogin = Environment.GetEnvironmentVariable(EnvironmentVariables.TEST_USER_LOGIN);
            var userPassword = Environment.GetEnvironmentVariable(EnvironmentVariables.TEST_USER_PASSWORD);

            if (request.queryStringParameters.TryGetValue("login", out var login) && request.queryStringParameters.TryGetValue("password", out var password))
            {
                if (login != userLogin || password != userPassword)
                {
                    return new ForbiddenResponse();
                }
            }
            else
            {
                return new ForbiddenResponse();
            }

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            IBrowsingContext context = BrowsingContext.New(Configuration.Default.WithDefaultLoader());

            using (var client = new HttpClient()) ;
            var document = await context.OpenAsync(Environment.GetEnvironmentVariable(EnvironmentVariables.ENDPOINT));

            var subjects = document.QuerySelectorAll<IHtmlOptionElement>("select#ms_subj option")
                                   .Where(x => !string.IsNullOrEmpty(x.Label))
                                   .Select(x => new Subject
                                   {
                                       Id = int.Parse(x.Value),
                                       Name = x.Label?.Trim()
                                   });

            var courtNodes = document.QuerySelectorAll<IHtmlDivElement>("div.courtInfoCont").ToList();
            var trimChars = new char[] { '\n', '\t', ':', ' ' };
            var courtsDictionary = new Dictionary<string, List<Court>>();

            foreach (var courtNode in courtNodes)
            {
                var subjectName = courtNode.ChildNodes.Skip(1).FirstOrDefault()?.Text()?.Trim();
                var court = new Court
                {
                    Name = courtNode.PreviousElementSibling?.Text()?.Trim(),
                    Code = courtNode.ChildNodes.Skip(4).FirstOrDefault()?.Text()?.Trim(trimChars)
                };
                if (courtsDictionary.TryGetValue(subjectName, out var courts))
                {
                    courts.Add(court);
                }
                else
                {
                    courtsDictionary.Add(subjectName, new List<Court> { court });
                }
            }

            var result = new List<SubjectInfo>();
            foreach (var subject in subjects)
            {
                if (courtsDictionary.TryGetValue(subject.Name, out var courts))
                {
                    var res = new SubjectInfo
                    {
                        Subject = subject,
                        Courts = courts
                    };
                    result.Add(res);
                }
            }
            return new OkResponse(result);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
            return new InternalServerErrorResponse();
        }
    }
}