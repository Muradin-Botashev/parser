﻿using System.Net;

namespace Parser.Models.Responses;

public class MethodNotAllowedResponse : ErrorResponse
{
    public MethodNotAllowedResponse() : base(HttpStatusCode.MethodNotAllowed, "Неподдерживаемый HTTP метод")
    {
    }
}