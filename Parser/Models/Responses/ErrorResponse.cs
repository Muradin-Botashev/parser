﻿using System.Net;

namespace Parser.Models.Responses;

public class ErrorResponse : Response
{
    public ErrorResponse(HttpStatusCode statusCode, string error) : base(statusCode, new ErrorMessage(error))
    {
    }
}