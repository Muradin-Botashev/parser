﻿using System.Net;

namespace Parser.Models.Responses;

public class InternalServerErrorResponse : ErrorResponse
{
    public InternalServerErrorResponse() : base(HttpStatusCode.InternalServerError, "Ошибка сервера")
    {
    }
}