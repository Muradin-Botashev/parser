﻿using System.Net;

namespace Parser.Models.Responses;

public class OkResponse : Response
{
    public OkResponse(object body) : base(HttpStatusCode.OK, body)
    {
    }
}