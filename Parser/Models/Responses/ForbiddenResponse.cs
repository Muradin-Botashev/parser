﻿using System.Net;

namespace Parser.Models.Responses;

public class ForbiddenResponse : ErrorResponse
{
    public ForbiddenResponse() : base(HttpStatusCode.Forbidden, "Доступ запрещен")
    {
    }
}