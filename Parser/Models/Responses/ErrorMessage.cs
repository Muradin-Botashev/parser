﻿namespace Parser.Models.Responses;

public class ErrorMessage
{
    public ErrorMessage(string error)
    {
        Error = error;
    }

    public string Error { get; set; }
}