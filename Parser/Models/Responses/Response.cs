﻿using System.Net;

namespace Parser.Models.Responses;

public class Response
{
    public Response(HttpStatusCode statusCode, object body)
    {
        StatusCode = statusCode;
        Body = body;
    }

    public HttpStatusCode StatusCode { get; set; }
    public object Body { get; set; }
}