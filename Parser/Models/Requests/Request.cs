﻿namespace Parser.Models.Requests;

public class Request<TBody>
{
    public string httpMethod { get; set; }

    public TBody Body { get; set; }

    public Dictionary<string, string> headers { get; set; }

    public Dictionary<string, string[]> multiValueHeaders { get; set; }

    public Dictionary<string, string> queryStringParameters { get; set; }

    public Dictionary<string, string[]> multiValueQueryStringParameters { get; set; }

    public bool isBase64Encoded { get; set; }
}