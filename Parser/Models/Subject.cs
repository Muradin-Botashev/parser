﻿namespace Parser.Models;

public class Subject
{
    public string Name { get; set; }
    public int Id { get; set; }
}