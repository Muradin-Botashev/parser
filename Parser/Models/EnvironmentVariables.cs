﻿namespace Parser.Models
{
    public static class EnvironmentVariables
    {
        public const string ENDPOINT = "endpoint";
        public const string TEST_USER_LOGIN = "test_user_login";
        public const string TEST_USER_PASSWORD = "test_user_password";
    }
}