﻿namespace Parser.Models;

public class Court
{
    public string Name { get; set; }
    public string Code { get; set; }
}