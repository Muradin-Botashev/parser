﻿using System.Text.Json.Serialization;

namespace Parser.Models;

public class SubjectInfo
{
    public Subject Subject { get; set; }

    [JsonPropertyName("child_courts")]
    public List<Court> Courts { get; set; }
}